import React from "react";
import styled from "styled-components";
import { Route, Switch } from "react-router-dom";
import { DocumentForm, Main } from "./pages";

const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/documents/new" component={DocumentForm} />
      </Switch>
    </div>
  );
};

export default App;
