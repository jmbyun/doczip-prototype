/* eslint import/no-webpack-loader-syntax: off */
import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import template from "!!raw-loader!../../doc_templates/sales_contract.jinja2.html"; 
import templateStyles from "!!raw-loader!../../doc_templates/sales_contract.css";

const fillParent = `
  width: 100%;
  height: 100%;
`;

const Container = styled.div`
  ${fillParent}
`;

const DocumentFrameContainer = styled.div`
  ${fillParent}
  margin: 0 auto;
  padding: 1rem;
  max-width: 960px;
`;

const DocumentFrame = styled.iframe`
  ${fillParent}
  border: 0;
`

const escapeHtml = (text: string) => text
  .replace(/&/g, "&amp;")
  .replace(/</g, "&lt;")
  .replace(/>/g, "&gt;")
  .replace(/"/g, "&quot;")
  .replace(/'/g, "&#039;")
  .replace(/'/g, "&#039;")
  .replace(/ /g, "&nbsp;")
  .replace(/\n/g, "<br />");

interface SalesContractTemplateProps {
  formData: {
    location: string,
    land_category: string,
    land_area: string,
    right_of_use_type: string,
    right_of_use_ratio_numerator: string,
    right_of_use_ratio_denominator: string,
    building_structure: string,
    building_purpose: string,
    building_area: string,
    sales_price: string,
  }
};

const SalesContractTemplate = ({ formData }: SalesContractTemplateProps) => {
  const frame = useRef<HTMLIFrameElement>(null);
  useEffect(() => {
    if (frame.current !== null) {
      const frameEl : HTMLIFrameElement = frame.current;
      if (frameEl.contentWindow !== null) {
        if (frameEl.contentWindow.document.body.childNodes.length === 0) {
          // Write HTML code in the IFrame.
          frameEl.contentWindow.document.open();
          frameEl.contentWindow.document.write(template);
          frameEl.contentWindow.close();
          // Set styles.
          const styleEl = document.createElement("style");
          styleEl.innerHTML = templateStyles;
          frameEl.contentWindow.document.getElementsByTagName("head")[0].appendChild(styleEl);
        }
        // Fill in the blanks.  
        for (const [key, value] of Object.entries(formData)) {
          const eid = `data__${key.replaceAll('_', '-')}`;
          const el = frameEl.contentWindow.document.getElementById(eid);
          if (el !== null) {
            if (value) {
              el.innerHTML = escapeHtml(value);
            } else {
              el.innerHTML = "<span style=\"color:red\">🐔?</span>";
            }
          }
        }
      }
    }
  })
  return (
    <Container>
      <DocumentFrameContainer>
        <DocumentFrame ref={frame} src="about:blank" />
      </DocumentFrameContainer>
    </Container>
  );
};

export default SalesContractTemplate;