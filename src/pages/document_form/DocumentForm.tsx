import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import styled, { css } from "styled-components";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import TextField from "@material-ui/core/TextField";
import SalesContractTemplate from "./SalesContractTemplate";

const headerHeight = `3rem`;

const ViewContainer = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  padding-top: ${headerHeight};
`;

const HeaderRow = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: ${headerHeight};
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #eee;
`;

const flexRowContainer = `
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`;

const flexItem = `
  display: block;
  flex: 0 0 auto;
`;

const flexFillItem = `
  display: block;
  flex: 1 1 auto;
`;

const fillParent = `
  position: relative;
  width: 100%;
  height: 100%;
`;

const scrollable = `
  overflow-x: none;
  overflow-y: auto;
`;

const ListItem = styled.div`
  padding: 0.5rem 0.5rem;
  font-size: 14px;
  line-height: 14px;
`;

const Icon = styled.div`
  display: inline-block;
  padding-right: 0.25rem;
`;

const HeaderItemGroup = styled.div`
  ${flexItem}
  ${flexRowContainer}
  padding: 0 1rem;
`;

const HeaderItem = styled.div`
  ${flexItem}
  padding-left: 0.5rem;
  &:first-of-type {
    padding-left: 0;
  }
`;

const MainRow = styled.div`
  ${fillParent}
  ${flexRowContainer}
`;

const DocListContainer = styled.div`
  ${flexItem}
  flex-basis: 12%;
  min-width: 240px;
  max-width: 450px;
  border-right: 1px solid #eee;
  height: 100%;
`;

const DocListInnerContainer = styled.div`
  ${fillParent}
  ${scrollable}
`;

const DocListItem = styled(ListItem)`
  cursor: pointer;
  &:hover { 
    background-color: #eee;
  }
`;

const FormContainer = styled.div`
  ${flexItem}
  padding: 1rem;
  flex-basis: 30%;
  max-width: 600px;
  border-right: 1px solid #eee;
  height: 100%;
`;

const VPaddedContainer = styled.div`
  padding: 0.5rem 0;
`;

const PreviewContainer = styled.div`
  ${flexFillItem}
  height: 100%;
`;

const Content = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1024px;
  padding: 1rem 1rem;
`;

const formDataInfos = [
  { key: "location", label: "소재지" },
  { key: "land_category", label: "지목" },
  { key: "land_area", label: "토지면적" },
  { key: "right_of_use_type", label: "대지권종류" },
  { key: "right_of_use_ratio_numerator", label: "대지권비율 분자" },
  { key: "right_of_use_ratio_denominator", label: "대지권비율 분모" },
  { key: "building_structure", label: "건물 구조" },
  { key: "building_purpose", label: "건물 용도" },
  { key: "building_area", label: "건물 면적" },
  { key: "sales_price", label: "매매대금" },
];

const DocumentForm = () => {
  const initState = Object.fromEntries(formDataInfos.map(({ key, label }) => [key, ""]))
  const [formData, setFormData] = useState(initState);
  const [autoFilled, setAutoFilled] = useState(false);
  const setFormDataValue = (key: string, value: string) => {
    setFormData({...formData, [key]: value});
  };

  const formControls = formDataInfos.filter(({ key }) => key !== "location").map(({ key, label }) => (
    <VPaddedContainer key={key}>
      <FormControl fullWidth>
        <FormLabel>
          {label}
        </FormLabel>
        <TextField
          variant="outlined"
          onChange={e => setFormDataValue(key, e.target.value)}
          value={formData[key]}
          disabled={!autoFilled}
          placeholder={autoFilled ? "": "자동입력을 먼저 해주세요."}
          fullWidth
        />
      </FormControl>
    </VPaddedContainer>
  ));

  const salesContractTemplateProps = {
    formData: {
      location: formData.location,
      land_category: formData.land_category,
      land_area: formData.land_area,
      right_of_use_type: formData.right_of_use_type,
      right_of_use_ratio_numerator: formData.right_of_use_ratio_numerator,
      right_of_use_ratio_denominator: formData.right_of_use_ratio_denominator,
      building_structure: formData.building_structure,
      building_purpose: formData.building_purpose,
      building_area: formData.building_area,
      sales_price: formData.sales_price,
    }
  };

  const handleAutoFill = () => {
    setAutoFilled(true);
    setFormData({
      ...formData,
      land_category: "닭집",
      land_area: "1234",
    })
  };

  return (
    <ViewContainer>
      <HeaderRow>
        <HeaderItemGroup>
          <HeaderItem>
            <NavLink to={"/"}>
              🐔🏠
            </NavLink>
          </HeaderItem>
          <HeaderItem>
            <NavLink to={"/"}>
              내 계약 목록 
            </NavLink>
          </HeaderItem>
          <HeaderItem>
            내 계약서 1
          </HeaderItem>
        </HeaderItemGroup>
        <HeaderItemGroup>
          계정 관리?
        </HeaderItemGroup>
      </HeaderRow>
      <MainRow>
        <DocListContainer>
          <DocListInnerContainer>
            <DocListItem>
              1. 부동산의 표시
            </DocListItem>
            <DocListItem>
              2.1. 계약내용: 매매대금
            </DocListItem>
            <DocListItem>
              2.2-2.8. 계약내용: 기타
            </DocListItem>
            <DocListItem>
              특약사항
            </DocListItem>
            <DocListItem>
              계약 당사자
            </DocListItem>
          </DocListInnerContainer>
        </DocListContainer>
        <FormContainer>
          <VPaddedContainer>
            소재지를 입력하고 버튼을 눌러 가능한 내용을 모두 자동입력하세요.
          </VPaddedContainer>
          <VPaddedContainer>
            <FormControl fullWidth>
              <FormLabel>
                소재지
              </FormLabel>
              <TextField
                variant="outlined"
                onChange={e => setFormDataValue('location', e.target.value)}
                value={formData.location}
                fullWidth
              />
            </FormControl>
          </VPaddedContainer>
          <VPaddedContainer>
            <FormControl fullWidth>
              <Button 
                variant="contained" 
                color="primary"
                onClick={handleAutoFill}
              >
                내용 자동입력
              </Button>
            </FormControl>
          </VPaddedContainer>
          {formControls}
          ...<br />
          ...
        </FormContainer>
        <PreviewContainer>
          <SalesContractTemplate {...salesContractTemplateProps} />
        </PreviewContainer>
      </MainRow>
    </ViewContainer>
  );
};

export default DocumentForm;