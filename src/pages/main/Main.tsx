import { DirectionsSubwayOutlined } from "@material-ui/icons";
import React from "react";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import Header from "../../layouts/Header";

const Container = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
`;

const Content = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1024px;
  padding: 1rem 1rem;
`;

const Main = () => {
  return (
    <Container>
      <Header />
      <Content>
        <NavLink to={"/documents/new"}>
          서류 작성
        </NavLink>
      </Content>
    </Container>
  );
};

export default Main;