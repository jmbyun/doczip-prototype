import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const headerHeight = `4.5rem`;

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  height: ${headerHeight};
  display: flex;
  position: relative;
  width: 100%;
  border-bottom: 1px solid #ddd;
`;

const LogoContainer = styled.section`
  display: inline-flex;
  justify-content: flex-start;
  align-items: center;
  flex: 1 1 auto;
  width: 50%;
`;

const LogoInnerContainer = styled.div`
  display: inline-flex;
  align-items: center;
  overflow-x: auto;
  cursor: pointer;
  padding-left: 8rem;
`;

const TextLogo = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: #000;
`;

const MenuContainer = styled.section`
  justify-content: flex-end;
  display: inline-flex;
  flex: 1 1 auto;
  align-items: center;
`;

const MenuInnerContainer = styled.div`
  padding-right: 8rem;
  font-size: 1rem;
`;

const Header = () => {
  return (
    <Container>
      <LogoContainer>
        <NavLink to={"/"}>
          <LogoInnerContainer>
            <TextLogo>🐔🏠</TextLogo>
          </LogoInnerContainer>
        </NavLink>
      </LogoContainer>
      <MenuContainer>
        <NavLink to={"/"}>
          <MenuInnerContainer>
            Home
          </MenuInnerContainer>
        </NavLink>
      </MenuContainer>
    </Container>
  );
};

export default Header;